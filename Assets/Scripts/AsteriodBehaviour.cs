using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class AsteriodBehaviour : MonoBehaviour
{
    [SerializeField] protected float m_AsteriodSpeed;

    [SerializeField] protected Sprite m_BigAsteriodSprite;

    [SerializeField] protected Sprite m_MediumAsteriodSprite;

    [SerializeField] protected Sprite m_SmallAsteriodSprite;

    [SerializeField] protected int m_BigAsteriodScore;

    [SerializeField] protected int m_MediumAsteriodScore;

    [SerializeField] protected int m_SmallAsteriodScore;

    private bool enteredViewSpace = false;

    public enum ASTERIODSIZE
    {
        BIG, MEDIUM, SMALL
    }

    private ASTERIODSIZE asteriodSize;

    private Rigidbody2D asteriodRigidbody2D;
    private Vector3 directionNorm;

    // Start is called before the first frame update
    void Start()
    {
        asteriodRigidbody2D = GetComponent<Rigidbody2D>();
        directionNorm = (Random.insideUnitSphere * 5f - transform.position).normalized;

        // Getting random asteriod size;
        var values = System.Enum.GetValues(typeof(ASTERIODSIZE));
        asteriodSize = (ASTERIODSIZE) Random.Range(0, values.Length);

        // Setting the respective image to its sprite renderer.
        GetComponent<SpriteRenderer>().sprite = GetAsteriodSprite(asteriodSize);
        Vector2 S = gameObject.GetComponent<SpriteRenderer>().sprite.bounds.size;
        gameObject.GetComponent<BoxCollider2D>().size = S;

        Invoke("DestroyAsteroid", 20f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(directionNorm.x, directionNorm.y, 0f) * Time.deltaTime * m_AsteriodSpeed);
    }

    void OnBecameVisible()
    {
        enteredViewSpace = true;
    }

    private void OnBecameInvisible()
    {
        if (enteredViewSpace)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            Debug.Log("Collided with " + collision.gameObject.name);
            Destroy(gameObject);
            Destroy(collision.gameObject);
            int score = asteriodSize == ASTERIODSIZE.BIG ? m_BigAsteriodScore : (asteriodSize == ASTERIODSIZE.MEDIUM ? m_MediumAsteriodScore : m_SmallAsteriodScore);
            GameController.Instance.PlayExplosionSound();
            GameController.Instance.AddScore(score);
            if (Random.Range(0, 10) < 2)
                GameController.Instance.AddPowerup(transform.position);
        }

        if (collision.CompareTag("Player"))
        {
            // Skip playing explosion sound when the ship's shield is activated.
            if (!collision.GetComponent<SpaceShipController>().IsShieldActivated)
                GameController.Instance.PlayExplosionSound();
        }
    }

    /// <summary>
    /// Destroys this game object.
    /// </summary>
    private void DestroyAsteroid()
    {
        DestroyImmediate(gameObject);
    }

    /// <summary>
    /// Returns the sprite image of Asteriod from the size passed as parameter.
    /// </summary>
    /// <param name="size"></param>
    /// <returns></returns>
    private Sprite GetAsteriodSprite(ASTERIODSIZE size)
    {
        Sprite sprite = default;
        switch (size)
        {
            case ASTERIODSIZE.BIG:
                sprite = m_BigAsteriodSprite;
                break;

            case ASTERIODSIZE.MEDIUM:
                sprite = m_MediumAsteriodSprite;
                break;

            case ASTERIODSIZE.SMALL:
                sprite = m_SmallAsteriodSprite;
                break;

            default:
                sprite = default;
                break;
        }

        return sprite;
    }
}