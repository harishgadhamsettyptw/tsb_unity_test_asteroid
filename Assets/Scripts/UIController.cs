using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : Singleton<UIController>
{
    [SerializeField] protected GameObject m_StartGamePanel;

    [SerializeField] protected Button m_StartGameButton;

    [SerializeField] protected GameObject m_GameOverTextObj;

    [SerializeField] protected Button m_RespawnButton;

    [SerializeField] protected Text m_ShipCountText;

    [SerializeField] protected Text m_ScoreText;

    // Start is called before the first frame update
    void Start()
    {
        // Hiding all the UI
        m_ShipCountText.text = string.Empty;
        m_ScoreText.text = string.Empty;

        m_StartGameButton.onClick.AddListener(()=> {
            StartButtonClicked();
        });

        m_RespawnButton.onClick.AddListener(() =>
       {
           RespawnClicked();
       });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void StartButtonClicked()
    {
        m_StartGamePanel.SetActive(false);
        GameController.Instance.StartGame();
    }

    public void ShowPanelOnGameOver()
    {
        m_GameOverTextObj.SetActive(true);
        m_StartGamePanel.SetActive(true);
    }

    /// <summary>
    /// Updated the ship count text in the UI with the current space ship count.
    /// </summary>
    /// <param name="noOfShipsLeft"></param>
    public void UpdateShipCountUI(int noOfShipsLeft)
    {
        m_ShipCountText.text = noOfShipsLeft.ToString();
    }

    /// <summary>
    /// Updates the score UI.
    /// </summary>
    /// <param name="totalScore"></param>
    public void UpdateScoreTextUI(int totalScore)
    {
        m_ScoreText.text = string.Format("Score : {0}", totalScore.ToString());
    }

    /// <summary>
    /// Show/ hides respawn button on UI panel.
    /// </summary>
    /// <param name="show"></param>
    public void ShowRespawnButton(bool show)
    {
        m_RespawnButton.gameObject.SetActive(show);
    }

    /// <summary>
    /// Spawn player's spaceship on button click.
    /// </summary>
    void RespawnClicked()
    {
        GameController.Instance.SpawnPlayer();
        ShowRespawnButton(false);
    }
}
