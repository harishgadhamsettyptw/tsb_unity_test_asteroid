using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupBehaviour : MonoBehaviour
{
    [SerializeField] protected Sprite m_ShieldPowerSprite;

    [SerializeField] protected Sprite m_SplitBulletPowerSPrite;

    private enum POWER_TYPE { SHIELD, SPLIT_BULLET }
    private POWER_TYPE powerType;

    // Start is called before the first frame update
    void Start()
    {
        var values = System.Enum.GetValues(typeof(POWER_TYPE));
        powerType = (POWER_TYPE)Random.Range(0, values.Length);

        GetComponent<SpriteRenderer>().sprite = powerType == POWER_TYPE.SHIELD ? m_ShieldPowerSprite : m_SplitBulletPowerSPrite;

        Invoke("DestroyPowerupObject", 5f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (powerType == POWER_TYPE.SHIELD)
                collision.GetComponent<SpaceShipController>().EnableShield(GameController.Instance.PowerupActiveTime);
            else if (powerType == POWER_TYPE.SPLIT_BULLET)
                collision.GetComponent<SpaceShipController>().EnableSplitBulletPowerup(GameController.Instance.PowerupActiveTime);
            Destroy(gameObject);
        }
    }

    void DestroyPowerupObject()
    {
        Destroy(gameObject);
    }
}