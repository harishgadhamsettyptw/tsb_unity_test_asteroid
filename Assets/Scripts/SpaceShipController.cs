using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SpaceShipController : MonoBehaviour
{
    [SerializeField] private float m_MovementSpeed;

    [SerializeField] private float m_RotationSpeed;

    [SerializeField] protected GameObject m_BulletPrefab;

    [SerializeField] protected Transform m_BarrelTransform;

    [SerializeField] protected GameObject m_ShieldObject;

    [SerializeField] protected float m_InitialShieldActiveTime;

    private Rigidbody2D spaceShipRigidBody2D;

    private SpriteRenderer spaceshipRenderer;

    private bool isWrappingX = false;
    private bool isWrappingY = false;

    private bool shieldActivated = false;
    public bool IsShieldActivated { get { return shieldActivated; } }

    private bool splitBulletActivated = false;

    // Start is called before the first frame update
    void Start()
    {
        spaceShipRigidBody2D = GetComponent<Rigidbody2D>();
        spaceshipRenderer = GetComponent<SpriteRenderer>();
        EnableShield(m_InitialShieldActiveTime);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            // Turn player towards left
            transform.localRotation = Quaternion.Euler(0f, 0f, transform.localEulerAngles.z + Time.deltaTime * m_RotationSpeed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            // Turn player towards right
            transform.localRotation = Quaternion.Euler(0f, 0f, transform.localEulerAngles.z - Time.deltaTime * m_RotationSpeed);
        }

        if (Input.GetKey(KeyCode.W))
        {
            // Move player forward
            Vector3 movement = transform.up * Time.deltaTime * m_MovementSpeed;
            spaceShipRigidBody2D.MovePosition(transform.position + movement);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            // Move player backward
            Vector3 movement = -transform.up * Time.deltaTime * m_MovementSpeed;
            spaceShipRigidBody2D.MovePosition(transform.position + movement);
        }

        // Spawn bullet on space key press.
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!splitBulletActivated)
                Instantiate(m_BulletPrefab, m_BarrelTransform.position, transform.rotation);
            else
            {
                Vector3 euler = transform.eulerAngles;
                Quaternion leftQuaternion = Quaternion.Euler(euler.x, euler.y, euler.z + 3f);
                Quaternion rightQuaternion = Quaternion.Euler(euler.x, euler.y, euler.z - 3f);
                Instantiate(m_BulletPrefab, m_BarrelTransform.position, leftQuaternion);
                Instantiate(m_BulletPrefab, m_BarrelTransform.position, rightQuaternion);
            }
        }

        // Skip porting of spaceship when the ship is visible.
        if (IsSpaceshipVisible())
        {
            isWrappingX = false;
            isWrappingY = false;
            return;
        }

        if (isWrappingX && isWrappingY)
            return;

        var viewportPosition = Camera.main.WorldToViewportPoint(transform.position);
        var newPosition = transform.position;

        if (!isWrappingX && (viewportPosition.x > 1 || viewportPosition.x < 0))
        {
            newPosition.x = -newPosition.x;
            isWrappingX = true;
        }

        if (!isWrappingY && (viewportPosition.y > 1 || viewportPosition.y < 0))
        {
            newPosition.y = -newPosition.y;
            isWrappingY = true;
        }

        transform.position = newPosition;
    }

    /// <summary>
    /// Returns the enable status of spaceship visibility.
    /// </summary>
    /// <returns></returns>
    private bool IsSpaceshipVisible()
    {
        return spaceshipRenderer.isVisible;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (shieldActivated)
            return;

        if (collision.CompareTag("Asteroid") || collision.CompareTag("UFO") || collision.CompareTag("UFOBullet"))
        {
            Debug.Log("Collided with " + collision.gameObject.name);
            Destroy(gameObject);
            Destroy(collision.gameObject);
            GameController.Instance.SpaceshipDestroyed();
        }
    }

    /// <summary>
    /// Enables the shield powerup
    /// </summary>
    /// <param name="enableTime"></param>
    public void EnableShield(float enableTime)
    {
        // Skip enabling shield when its already activated.
        if (shieldActivated)
            return;

        shieldActivated = true;
        m_ShieldObject.SetActive(true);
        CancelInvoke("DisableShield");
        Invoke("DisableShield", enableTime);
    }

    /// <summary>
    /// Disable shield powerup
    /// </summary>
    void DisableShield()
    {
        shieldActivated = false;
        m_ShieldObject.SetActive(false);
    }

    /// <summary>
    /// Enable slipbullet powerup
    /// </summary>
    /// <param name="enableTime"></param>
    public void EnableSplitBulletPowerup(float enableTime)
    {
        splitBulletActivated = true;
        CancelInvoke("DisableSplitBulletPowerup");
        Invoke("DisableSplitBulletPowerup", enableTime);
    }

    /// <summary>
    /// Disable slipbullet powerup
    /// </summary>
    void DisableSplitBulletPowerup()
    {
        splitBulletActivated = false;
    }
}