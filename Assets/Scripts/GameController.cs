using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class GameController : Singleton<GameController>
{
    [SerializeField] protected GameObject m_PlayerPrefab;

    [SerializeField] protected GameObject m_AsteriodPrefab;

    [SerializeField] protected float m_AsteriodSpawnInterval;

    [SerializeField] protected int m_TotalShipsLeft;

    [SerializeField] protected AudioClip m_ExplosionAudioClip;

    [SerializeField] protected AudioClip m_GameLoseAudioClip;

    [SerializeField] protected GameObject m_PowerUpPrefab;

    [SerializeField] protected GameObject m_UFOPrefab;

    [SerializeField] protected float m_UFOSpawnInterval;

    [SerializeField] protected float m_PowerupActiveTime;

    public float PowerupActiveTime { get { return m_PowerupActiveTime; } }

    private Vector3 screenCenterInWorld;
    private float minX, minY, maxX, maxY;
    private float maxRange = 5f, minRange = 2f;

    private float timeAcc = 0f;
    private float timeAcc_UFO = 0f;

    private int totalScore = 0;

    private bool gameStarted = false;
    private int spaceshipsLeft;

    private GameObject spaceShipObj;

    // Start is called before the first frame update
    void Start()
    {
        screenCenterInWorld = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0f));

        minX = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, -Camera.main.transform.position.z)).x;
        minY = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, -Camera.main.transform.position.z)).y;

        maxX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, -Camera.main.transform.position.z)).x;
        maxY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, -Camera.main.transform.position.z)).y;
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameStarted)
            return;

        timeAcc += Time.deltaTime;
        if (timeAcc >= m_AsteriodSpawnInterval)
        {
            timeAcc -= m_AsteriodSpawnInterval;
            SpawnAsteriod();
        }

        timeAcc_UFO += Time.deltaTime;
        if (timeAcc_UFO >= m_UFOSpawnInterval)
        {
            timeAcc_UFO -= m_UFOSpawnInterval;
            SpawnUFOObject();
        }
    }

    /// <summary>
    /// Start the game by spawing the player and all the other game elements.
    /// </summary>
    public void StartGame()
    {
        gameStarted = true;
        spaceshipsLeft = m_TotalShipsLeft;
        UIController.Instance.UpdateScoreTextUI(0);
        SpawnPlayer();
    }

    /// <summary>
    /// Resets all the variables to default values and shows the game over panel from UI controller.
    /// </summary>
    void GameOver()
    {
        gameStarted = false;
        timeAcc = 0f;
        totalScore = 0;
        UIController.Instance.ShowPanelOnGameOver();

        // Playing losing audio clip
        GetComponent<AudioSource>().clip = m_GameLoseAudioClip;
        GetComponent<AudioSource>().Play();
    }

    /// <summary>
    /// Spawing a new Asteroid after a time delay.
    /// </summary>
    void SpawnAsteriod()
    {
        Vector3 spawnPosition = Vector3.zero;

        if (Random.value > 0.5f)
            spawnPosition.x = Random.Range(minX - maxRange, minX - minRange);
        else
            spawnPosition.x = Random.Range(maxX + minRange, maxX + maxRange);

        if (Random.value > 0.5f)
        {
            // Spawn on top side of the screen
            spawnPosition.y = Random.Range(maxY + minRange, maxY + maxRange);
        }
        else
        {
            // Spawn on bottom side of the screen
            spawnPosition.y = Random.Range(minY - maxRange, minY - minRange);
        }

        GameObject asteroidObject = Instantiate(m_AsteriodPrefab, spawnPosition, Quaternion.identity);
    }

    void SpawnUFOObject()
    {
        Vector3 spawnPosition = Vector3.zero;

        if (Random.value > 0.5f)
            spawnPosition.x = Random.Range(minX - maxRange, minX - minRange);
        else
            spawnPosition.x = Random.Range(maxX + minRange, maxX + maxRange);

        if (Random.value > 0.5f)
        {
            // Spawn on top side of the screen
            spawnPosition.y = Random.Range(maxY + minRange, maxY + maxRange);
        }
        else
        {
            // Spawn on bottom side of the screen
            spawnPosition.y = Random.Range(minY - maxRange, minY - minRange);
        }

        Instantiate(m_UFOPrefab, spawnPosition, Quaternion.identity);
    }

    /// <summary>
    /// Play explosion sound.
    /// </summary>
    public void PlayExplosionSound()
    {
        GetComponent<AudioSource>().clip = m_ExplosionAudioClip;
        GetComponent<AudioSource>().Play();
    }

    /// <summary>
    /// Checks for life lines and enable respawn button when player's shaceship is destroyed.
    /// </summary>
    public void SpaceshipDestroyed()
    {
        if (spaceshipsLeft > 0)
        {
            UIController.Instance.ShowRespawnButton(true);
        }
        else
        {
            spaceshipsLeft = 3;
            GameOver();
        }
    }

    /// <summary>
    /// Spawning a player spaceship.
    /// </summary>
    public void SpawnPlayer()
    {
        --spaceshipsLeft;

        UIController.Instance.UpdateShipCountUI(spaceshipsLeft);
        if (spaceShipObj != null)
        {
            Destroy(spaceShipObj);
            spaceShipObj = null;
        }
        spaceShipObj = Instantiate(m_PlayerPrefab, Vector3.zero, Quaternion.identity);
    }

    /// <summary>
    /// Accumulating score each time bullet hits asteriod.
    /// </summary>
    /// <param name="score"></param>
    public void AddScore(int score)
    {
        totalScore += score;
        UIController.Instance.UpdateScoreTextUI(totalScore);
    }

    /// <summary>
    /// Instantiate powerup object at the position sent as parameter.
    /// </summary>
    /// <param name="position"></param>
    public void AddPowerup(Vector3 position)
    {
        Instantiate(m_PowerUpPrefab, position, Quaternion.identity);
    }
}