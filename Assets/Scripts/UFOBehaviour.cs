using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class UFOBehaviour : MonoBehaviour
{
    [SerializeField] protected float m_UFOSpeed;

    [SerializeField] protected GameObject m_BulletPrefab;

    [SerializeField] protected Transform m_BarrelTransform;

    [SerializeField] protected float m_BulletSpawnInterval;

    private Rigidbody2D ufoRigidbody2D;
    private Vector3 directionNorm;

    private bool enteredViewSpace = false;

    private float timeAcc = 0.0f;

    private GameObject playerObject;
    private Vector3 playerLastPos = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        ufoRigidbody2D = GetComponent<Rigidbody2D>();
        directionNorm = (Random.insideUnitSphere * 5f - transform.position).normalized;

        playerObject = GameObject.FindGameObjectWithTag("Player");

        Invoke("DestroyUFO", 20f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(directionNorm.x, directionNorm.y, 0f) * Time.deltaTime * m_UFOSpeed;

        if (!enteredViewSpace)
            return;

        if (playerObject == null)
            playerObject = GameObject.FindGameObjectWithTag("Player");

        if (playerObject == null)
            return;

        timeAcc += Time.deltaTime;
        if (timeAcc >= m_BulletSpawnInterval)
        {
            timeAcc -= m_BulletSpawnInterval;

            float angleZ = Vector2.Angle(transform.up, playerLastPos - transform.position);
            transform.Rotate(transform.forward, angleZ);
            Instantiate(m_BulletPrefab, m_BarrelTransform.position, transform.rotation);
        }
    }

    void OnBecameVisible()
    {
        enteredViewSpace = true;
    }

    private void OnBecameInvisible()
    {
        if (enteredViewSpace)
        {
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            Debug.Log("Collided with " + collision.gameObject.name);
            Destroy(gameObject);
            Destroy(collision.gameObject);
            GameController.Instance.PlayExplosionSound();
            GameController.Instance.AddScore(200);
            if (Random.Range(0, 10) < 1)
                GameController.Instance.AddPowerup(transform.position);
        }

        if (collision.CompareTag("Player"))
        {
            // Skip playing explosion sound when the ship's shield is activated.
            if (!collision.GetComponent<SpaceShipController>().IsShieldActivated)
                GameController.Instance.PlayExplosionSound();
        }
    }

    /// <summary>
    /// Destroys this game object.
    /// </summary>
    private void DestroyUFO()
    {
        DestroyImmediate(gameObject);
    }
}
