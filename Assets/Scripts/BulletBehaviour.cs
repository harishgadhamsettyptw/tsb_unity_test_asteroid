using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(AudioSource))]
public class BulletBehaviour : MonoBehaviour
{
    [SerializeField] protected float m_BulletSpeed;

    private Rigidbody2D bulletRigidbody2D;

    // Start is called before the first frame update
    void Start()
    {
        bulletRigidbody2D = GetComponent<Rigidbody2D>();
        Invoke("DestroyBullet", 3f);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = transform.up * Time.deltaTime * m_BulletSpeed;
        bulletRigidbody2D.MovePosition(transform.position + movement);
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    void DestroyBullet()
    {
        Destroy(gameObject);
    }
}